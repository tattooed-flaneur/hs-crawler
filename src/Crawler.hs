{-# Language LambdaCase #-}
{-# Language OverloadedStrings #-}
module Crawler
where

import Control.Monad
import Data.Maybe (mapMaybe)
import qualified Data.ByteString.Lazy as BSL
import Text.HTML.Scalpel
import Data.Aeson as Aeson
import Database.PostgreSQL.Simple
import Control.Concurrent.ParallelIO.Local

threads ::Int
threads = 100

scrapFlaneur :: IO [(URL, BSL.ByteString, Aeson.Value)]
scrapFlaneur = withPool threads $ \threadPool -> do
    topLinks >>= \case
        Nothing -> error $ "cannot scrap " ++  entry
        Just links -> do
            list <- parallelInterleavedE threadPool $ map (scrapBeitrag . (++) baseURL) links
            return $ concat $ discardExceptions list
{-
-- for testing : this is the single threaded version
            list <- sequence $ map (scrapBeitrag . (++) baseURL) links -- single threaded version
            return $ concat list
-}
    where
        baseURL = "https://www.der-flaneur.rocks"
        entry = baseURL ++ "/category/ausgehen.html"
        topLinks :: IO (Maybe [URL])
        topLinks = scrapeURL entry $ attrs "href" $ "h1" // "a"
        discardExceptions = mapMaybe $ either (const Nothing) Just

scrapBeitrag :: URL -> IO [(URL, BSL.ByteString, Aeson.Value)]
scrapBeitrag url = trace url $ do
    d <- scrapeURL url scraper
    case d of
        Nothing -> do
            putStrLn $ "cannot scrap " ++ url
            error $ "cannot scrap " ++ url
        Just j -> return j
    where
        scraper = do
            jlist <- texts $ "script" @: [ "type" @= "application/ld+json"]
            case forM jlist Aeson.decode of
                Nothing -> return []
                Just objs -> return $ zip3 (repeat url) jlist objs

trace :: String -> IO a -> IO a
trace msg action = do
    putStrLn $ "Start :" ++ msg
    res <- action
    putStrLn $ "Stop :" ++ msg
    return res

testUpload :: IO ()
testUpload = do
    conn <- connectPostgreSQL "" -- Reads from ENV: PGHOST,PGUSER,PGPASSWORD
    scrap <- scrapFlaneur
    forM_ scrap $ \(url,_rawData,json) -> do
        query conn
              "INSERT INTO entities(url,type,data) values (?, 'XEvent', ?)"
              (url,json):: IO [Only ()]

sqlTest :: IO ()
sqlTest = do
  conn <- connectPostgreSQL ""
  res <- query_ conn "SELECT id,url,type,data FROM entities" :: IO [(Int,String,String,Aeson.Value)]
  forM_ res print
