{ mkDerivation, aeson, base, bytestring, parallel-io
, postgresql-simple, scalpel, stdenv
}:
mkDerivation {
  pname = "hs-crawler";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base bytestring parallel-io postgresql-simple scalpel
  ];
  executableHaskellDepends = [
    aeson base bytestring postgresql-simple scalpel
  ];
  homepage = "https://gitlab.com/tattooed-flaneur";
  description = "saar hackathon event crawler";
  license = stdenv.lib.licenses.bsd3;
}
